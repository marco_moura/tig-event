# Tig Events

An application that listens to Github Events via webhooks and expose by an api for later use.

![alt text](imgs/octo_events.png)

 The test consists in building 2 endpoints:

## 1. Webhook Endpoint

The Webhook endpoint receives events from Github and saves them on the database, in order to do that you must read the following docs:

* Webhooks Overview: https://developer.github.com/webhooks/
* Creating Webhooks : https://developer.github.com/webhooks/creating/

It must be called `/events`

## adicionar events
`GET /events`

**Response**

> 201 OK
```json
  {
    "id": 1,
    "external_id": 330887484,
    "uri": "/issues/1/events"
  }
```

## 2. Events Endpoint

The Events endpoint will expose the persist the events by an api that will filter by issue number

**Request:**

curl localhost:4000/issues/1/events

**Response:**

```json
$ curl -v localhost:4000/issues/2/events

> GET /issues/2/events HTTP/1.1
> User-Agent: curl/7.38.0
> Host: localhost:4000
> Accept: */*
>
< HTTP/1.1 200 OK
< Content-Type: application/json; charset=utf-8
< ETag: W/"682cc1be02da896e3f0a83934c0c8ff7"
<
[
  {
    "action": "opened",
    "created_at": "2018-06-10T00:39:06.868Z"
  },
  {
    "action": "closed",
    "created_at": "2018-06-10T00:39:26.336Z"
  },
  {
    "action": "reopened",
    "created_at": "2018-06-10T00:39:32.809Z"
  },
  {
    "action": "created",
    "created_at": "2018-06-10T00:39:39.017Z"
  },
  {
    "action": "deleted",
    "created_at": "2018-06-10T00:39:48.488Z"
  }
]
```

## Rodar a aplicação

* bundle install
* rails db:setup
* rails s -p 4000
* ngrok http 4000


* necessário configurar o github para apontar para algo como

- http://15b31aad.ngrok.io/events


**Github Integration Instructions**

* Tip: You can use ngrok (https://ngrok.com/)  to install / debug the webhook calls, it generates a public url that will route to your local host:

   $ sudo ngrok http 4000

![alt text](imgs/ngrok.png)

   GitHub

![alt text](imgs/add_webhook.png)

**Final Observations**

* Use any library / framework / gem  you want, you don't have to do anything "from scratch"
* Write tests, use your favorite framework for that
* Use Postgres 9.6 or MySQL 5.7 as database;
* Add to README.md your instructions for running the project. Whether you're delivering just source code or an accompanying `Dockerfile`/`docker-compose.yml`, we expect at most the following commands to be needed to run your solution (besides the usual docker related deploy steps):
    - `rake db:create`
    - `rake db:migrate`
    - `rails s -p 3000 -b '0.0.0.0'`
* We'll run your code with Ruby 2.5.1;
* Success and have fun :-)
