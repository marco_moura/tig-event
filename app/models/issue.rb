class Issue < ApplicationRecord
  validates_presence_of :external_id

  has_many :events, inverse_of: :issue
  accepts_nested_attributes_for :events
end
