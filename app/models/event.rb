class Event < ApplicationRecord
  belongs_to :issue, inverse_of: :events

  validates_presence_of :issue, :payload, :action
end
