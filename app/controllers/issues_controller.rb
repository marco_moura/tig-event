class IssuesController < ApplicationController
    # GET /issue/1/events
  def show
    # FIXME, renomear para eventos
    # @events = EventsFacade.find params[:id]
    @events = IssueFacade.show params[:id]
    render json: @events
  rescue IssueFacade::IssueNotFoundError  => e
    render json: {error: e.message}, status: :not_found
  end
end
