class EventsController < ApplicationController
  # POST /events
  def create
    @issue = EventFacade.create! request.POST

    render json: @issue, status: :created, location: issue_url(@issue.id)
  rescue EventFacade::EventInvalidError => e
    render json: e.message, status: :unprocessable_entity
  end
end
