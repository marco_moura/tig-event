class IssueFacade
  def self.show(id)
    events = Issue.find(id).events

    EventPresenter.present events
  rescue ActiveRecord::RecordNotFound => e
    raise IssueNotFoundError, e.message
  end

  class IssueNotFoundError < StandardError; end
end
