class EventFacade
  # TODO, strategy to take care of many type of events
  # https://developer.github.com/v3/activity/events/types/
  # Events::IssueBuilder.attributes params
  # Events::ForkEventBuilder.attributes params
  #
  # Events::BuildStrategy.attributes params
  def self.create!(params)
    attributes = Events::IssueBuilder.attributes params

    issue = Issue.find_or_initialize_by external_id: attributes[:external_id]
    issue.events_attributes = attributes[:events_attributes]
    issue.save!

    IssuePresenter.present issue
  rescue ActiveRecord::RecordInvalid => e
    raise EventInvalidError, e.message
  end

  class EventInvalidError < StandardError; end
end
