class EventPresenter
  def self.present(events)
    events.collect do |event|
      new event
    end
  end

  def initialize(event)
    @event = event
  end

  def as_json(_)
    {
      action: @event.action,
      created_at: @event.created_at
    }
  end
end
