class IssuePresenter
  def self.present(issue)
    new issue
  end

  def initialize(issue)
    @issue = issue
  end

  def id
    @issue.id
  end

  def as_json(_)
    {
      id: @issue.id,
      external_id: @issue.external_id,
      uri: "/issues/#{@issue.id}/events"
    }
  end
end
