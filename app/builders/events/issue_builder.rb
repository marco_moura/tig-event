module Events
  class IssueBuilder
    def self.attributes(params)
      new(params).attributes
    end

    def initialize(params)
      @params = params
    end

    def attributes
      {
        external_id: "issue:#{@params['issue']['id']}",
        events_attributes: [event]
      }
    end

    private

    def event
      {
        action: @params['action'],
        payload: @params
      }
    end
  end
end
