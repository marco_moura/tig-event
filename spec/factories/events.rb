FactoryBot.define do
  factory :event do
    payload '{fa: :ke}'
    action 'open'

    issue
  end
end
