FactoryBot.define do
  factory :issue do
    external_id 'issue:'

    after(:create) do |issue, _|
      # Needs to save on DB to have an id
      issue.update_attribute :external_id, "issue:#{issue.id}"
    end

    transient do
      events_count 2
    end

    after(:create) do |issue, evaluator|
      create_list(:event, evaluator.events_count, issue: issue)
    end
  end
end
