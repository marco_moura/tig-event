require 'rails_helper'

describe EventPresenter do
  subject { described_class.new fake_event }
  let(:fake_event) { double attributes }
  let(:attributes) { {action: 'fake', created_at: 'asdf'} }

  it { expect(subject.as_json('')).to eq attributes }
end
