require 'rails_helper'

describe EventFacade do
  subject { described_class.create! fa: :ke }

  before do
    allow(Events::IssueBuilder).to receive(:attributes).and_return external_id: :fake
    allow(Issue).to receive(:find_or_initialize_by).and_return fake_issue
  end

  let(:fake_issue) do
    double 'events_attributes=': nil
  end

  context 'when creates with success' do
    before do
      allow(fake_issue).to receive :save!
      allow(IssuePresenter).to receive :present
    end

    it { expect { subject }.to_not raise_error }
  end

  context 'when fails to create' do
    before { allow(fake_issue).to receive(:save!).and_raise ActiveRecord::RecordInvalid }

    it { expect { subject }.to raise_error EventFacade::EventInvalidError }
  end
end
