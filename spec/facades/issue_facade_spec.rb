require 'rails_helper'

describe IssueFacade do
  subject { described_class.show 171 }

  before { allow(Issue).to receive(:find).and_return events }

  let(:events) { double 'FakeEvents' }

  context 'when creates with success' do
    before do
      allow(events).to receive :events
      allow(EventPresenter).to receive :present
    end

    it { expect { subject }.to_not raise_error }
  end

  context 'when fails to create' do
    before { allow(events).to receive(:events).and_raise ActiveRecord::RecordNotFound }

    it { expect { subject }.to raise_error IssueFacade::IssueNotFoundError }
  end
end
