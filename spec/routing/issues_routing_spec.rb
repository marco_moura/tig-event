require 'rails_helper'

describe IssuesController, type: :routing do
  it { expect(get: '/issues/171/events').to route_to controller: 'issues', action: 'show', id: '171' }
end
