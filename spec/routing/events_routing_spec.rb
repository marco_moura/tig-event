require "rails_helper"

describe EventsController, type: :routing do
  it { expect(post: '/events').to route_to 'events#create' }
end
