require 'rails_helper'

RSpec::Matchers.define_negated_matcher :not_change, :by

describe 'POST /events', type: :request do
  context 'successfully creates the event' do
    let(:params) do
      {
        'action' => 'opened',
        'issue' => {
          "id" => 330887484,
          "state" => "open"
        }
      }
    end

    let(:body_expected) do
      {
        'id' => 1,
        'external_id' => "issue:#{330887484}",
        'uri' => "/issues/1/events"
      }
    end

    it 'respondes 201 (Created) with a body JSON', :aggregate_failures do
      expect { post events_path, params: params }
        .to change(Issue, :count).by(1)
        .and change(Event, :count).by 1

      expect(response).to have_http_status(201)
      expect(ActiveSupport::JSON.decode response.body).to eq body_expected
    end
  end

  context 'successfully add an event to a existent issue' do
    let!(:issue) { create :issue }

    let(:params) do
      {
        "action" => "closed",
        "issue" => {
          "id" => issue.id,
          "state" => "close"
        }
      }
    end

    let(:body_expected) do
      {
        'id' => issue.id,
        'external_id' => "issue:#{issue.id}",
        'uri' => "/issues/#{issue.id}/events"
      }
    end

    it 'respondes 201 (Created) with a body JSON', :aggregate_failures do
      expect { post events_path, params: params }
        .to change(Event, :count).by 1

      expect(response).to have_http_status(201)
      expect(ActiveSupport::JSON.decode response.body).to eq body_expected
    end
  end
end
