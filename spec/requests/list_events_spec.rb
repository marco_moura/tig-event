require 'rails_helper'

describe 'GET /issues/1000/events', :time_travel, type: :request do
  before do
    travel_to Time.zone.local(2018, 6, 9, 20, 1, 2)
    get issue_path issue_id
  end

  context 'when the issue does not exist' do
    let(:issue_id) { 171 }

    it 'respondes 404 (Not Found) with erro json on body', :aggregate_failures do
      expect(response).to have_http_status 404
      expect(response.body).to eq "{\"error\":\"Couldn't find Issue with 'id'=171\"}"
    end
  end

  context 'when the issue exists' do
    let(:issue_id) { create(:issue).id }

    let(:expected_output) do
      [
        {'action' => 'open', 'created_at' => '2018-06-09T20:01:02.000Z'},
        {'action' => 'open', 'created_at' => '2018-06-09T20:01:02.000Z'}
      ]
    end

    it 'respondes 200 (OK) with a body JSON', :aggregate_failures do
      expect(response).to have_http_status 200
      expect(ActiveSupport::JSON.decode response.body).to eq expected_output
    end
  end
end
