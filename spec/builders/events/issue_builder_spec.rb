require 'rails_helper'

describe Events::IssueBuilder do
  context '#attributes' do
    subject { described_class.new expected_input }

    let(:expected_input) do
      {
        'action' => 'opened',
        'issue' => {
          'id' => 171,
          'state' => 'closed'
        }
      }
    end

    let(:expected_output) do
      {
        external_id: "issue:#{expected_input['issue']['id']}",
        events_attributes: [{ action: 'opened', payload: expected_input }]
      }
    end

    it { expect(subject.attributes).to eq expected_output }
  end
end

