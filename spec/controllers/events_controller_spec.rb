require 'rails_helper'

describe EventsController, type: :controller do
  describe 'POST #create' do
    context 'with valid params' do
      context 'successfully created' do
        before do
          allow(EventFacade)
            .to receive(:create!)
            .and_return double(id: 1, to_json: fake_json)

          post :create, params: {}
        end
        let(:fake_json) { '{"fake": "json"}' }

        it 'renders a JSON response with the issue and a header with location', :aggregate_failures do
          expect(response).to have_http_status :created
          expect(response.content_type).to eq 'application/json'
          expect(response.body).to eq fake_json
          expect(response.location).to eq issue_url 1
        end
      end
    end

    context 'with invalid params' do
      before do
        allow(EventFacade).to receive(:create!).and_raise EventFacade::EventInvalidError
        post :create, params: {}
      end

      it 'renders a JSON response with errors for the new issue', :aggregate_failures do
        expect(response).to have_http_status :unprocessable_entity
        expect(response.content_type).to eq 'application/json'
      end
    end
  end
end
