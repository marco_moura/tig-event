require 'rails_helper'

describe IssuesController, type: :controller do
  describe "GET #show" do
    context 'when is successfully' do
      before do
        allow(IssueFacade).to receive(:show).and_return(double(to_json: fake_json))
        get :show, params: {id: 171}
      end

      let(:fake_json) { '{"fake": "json"}' }

      it "returns a success response" do
        expect(response).to be_successful
        expect(response.content_type).to eq 'application/json'
        expect(response.body).to eq fake_json
      end
    end
  end

  context 'when it fails' do
    before do
      allow(IssueFacade).to receive(:show).and_raise IssueFacade::IssueNotFoundError.new('fake message')
      get :show, params: {id: 171}
    end

    let(:expected_output) { "{\"error\":\"fake message\"}" }

    it "returns a success response" do
      expect(response).to be_not_found
      expect(response.content_type).to eq 'application/json'
      expect(response.body).to eq expected_output
    end
  end
end
