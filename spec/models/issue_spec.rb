require 'rails_helper'

describe Issue, type: :model do
  it { is_expected.to validate_presence_of :external_id }
  it { is_expected.to accept_nested_attributes_for :events }
  it { is_expected.to have_many :events }
end
