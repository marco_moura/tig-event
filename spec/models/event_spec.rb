require 'rails_helper'

RSpec.describe Event, type: :model do
  it { is_expected.to belong_to :issue}
  it { is_expected.to validate_presence_of :issue }
  it { is_expected.to validate_presence_of :payload }
  it { is_expected.to validate_presence_of :action }
end
