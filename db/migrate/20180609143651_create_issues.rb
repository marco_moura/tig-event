class CreateIssues < ActiveRecord::Migration[5.2]
  def change
    create_table :issues do |t|
      t.string :external_id, not_null: false

      t.timestamps
    end
    add_index :issues, :external_id, unique: true
  end
end
