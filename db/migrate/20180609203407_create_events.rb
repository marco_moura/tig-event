class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.json :payload, not_null: false
      t.string :action, not_null: false
      t.references :issue, foreign_key: true

      t.timestamps
    end
  end
end
