Rails.application.routes.draw do
  post '/events', to: 'events#create'
  get '/issues/:id/events', to: 'issues#show', as: :issue
end
